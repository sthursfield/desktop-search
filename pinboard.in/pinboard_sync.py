#!/usr/bin/env python3
# Copyright (C) 2020  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pinboard
import posixfs
import xdg.BaseDirectory
import whoosh.fields
import whoosh.filedb.filestore

import argparse
import configparser
import datetime
import json
import logging
import pathlib
import sys

log = logging.getLogger()

CONFIG_NAME = 'uk.me.afuera.pinboard_sync.conf'
CACHE_NAME = 'uk.me.afuera.pinboard_sync'


# This must be kept in sync with pinboard_search_provider.py.
class PinboardSchema(whoosh.fields.SchemaClass):
    description = whoosh.fields.TEXT()
    extended = whoosh.fields.TEXT()
    tags = whoosh.fields.KEYWORD()
    hash = whoosh.fields.ID(unique=True, stored=True)
    href = whoosh.fields.STORED()


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Pinboard bookmarks sync tool")
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help="Enable detailed logging to stderr")
    return parser


def load_config():
    parser = configparser.ConfigParser()
    for configpath in xdg.BaseDirectory.load_config_paths(CONFIG_NAME):
        log.info("Loading config from %s", configpath)
        parser.read(configpath)
    try:
        api_token = parser['authentication']['api_token']
    except KeyError:
        raise RuntimeError("Did not find 'api_token' in config file. "
                           f"Check ~/.config/{CONFIG_NAME}")
    return api_token


def posts_list_to_dict(posts_list):
    """Turn list of posts into dict, with 'hash' as the key.

    This allows us to use 'hash' as an identifier for the post data.

    """
    posts_dict = {}
    for item in posts_list:
        hash = item['hash']
        posts_dict[hash] = item
    return posts_dict


def sync_posts(cachepath, api):
    """Check if there are new or modified posts at pinboard.in.

    We redownload the whole data set if it has changed. For my 11K bookmarks this
    is a ~7MB download.

    """
    posts_cache = cachepath.joinpath('posts.json')

    last_cache_update = None
    if posts_cache.exists():
        mtime = posts_cache.stat().st_mtime
        last_cache_update = datetime.datetime.fromtimestamp(mtime)

    last_remote_update = api.posts.update()

    log.info("Cache last updated: {}".format(last_cache_update))
    log.info("Remote last updated: {}".format(last_remote_update))

    if last_cache_update is None or last_remote_update > last_cache_update:
        logging.info("Fetching all posts")
        response = api.posts.all(parse_response=False)
        posts_list = json.load(response)
        posts_dict = posts_list_to_dict(posts_list)
        with posixfs.AtomicWritingText(posts_cache, durable=False) as f:
            json.dump(posts_dict, f)
        changed = True
    else:
        with open(posts_cache, 'r') as f:
            posts_dict = json.load(f)
        changed = False

    return posts_dict, changed


def recreate_index(cachepath, posts):
    """Create the search index from the posts data.

    We don't try to do incremental updates. With my 11K bookmarks, creating
    the index completes in a few seconds.

    """
    log.info("(Re)creating index")

    schema = PinboardSchema()
    storage = whoosh.filedb.filestore.FileStorage(cachepath)
    index = storage.create_index(schema)

    writer = index.writer()
    for post in posts.values():
        fields = {field:(post[field] or '') for field in schema.names()}
        writer.add_document(**fields)
    writer.commit()
    log.info("Successfully (re)created index")



def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    api_token = load_config()

    pb = pinboard.Pinboard(api_token)
    cachepath = pathlib.Path(xdg.BaseDirectory.save_cache_path(CACHE_NAME))

    posts, changed = sync_posts(cachepath, pb)
    if changed or not whoosh.index.exists_in(cachepath):
        recreate_index(cachepath, posts)
    else:
        log.info("Not updating index -- no changes.")


if __name__ == '__main__':
    try:
        main()
    except RuntimeError as e:
        sys.stderr.write("ERROR: {}\n".format(e))
        sys.exit(1)
