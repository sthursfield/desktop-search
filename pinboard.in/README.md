# Pinboard.in search provider for GNOME

To install from source, run these commands.

    meson configure --prefix=/usr
    sudo ninja install
    systemctl daemon-reload --user
    systemctl enable --user pinboard_sync.service

(You can install in a different prefix if you set `$XDG_DATA_DIRS` globally
to include it -- see https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3060).

Next, visit <https://pinboard.in/settings/password> and copy your API token.

Create a file named `$HOME/.config/uk.me.afuera.pinboard_sync.conf` like this:

    [authentication]
    api_token = me:1234abcd   # Replace with your token

Check the sync is working:

    systemctl status --user pinboard_sync.service

