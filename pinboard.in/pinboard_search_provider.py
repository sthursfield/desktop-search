#!/usr/bin/env python3
# Copyright (C) 2020  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import GLib
from gi.repository import Gio

# We use pydbus as GDBus can't publish services from Python,
# according to https://github.com/hcoin/pydbus
import pydbus
import pydbus.generic
import whoosh.fields
import whoosh.filedb.filestore
import whoosh.qparser
import xdg.BaseDirectory

import argparse
import logging
import json
import pathlib
import sys
import urllib.parse

log = logging.getLogger()

CACHE_NAME = 'uk.me.afuera.pinboard_sync'


# This must be kept in sync with pinboard_sync.py.
class PinboardSchema(whoosh.fields.SchemaClass):
    description = whoosh.fields.TEXT()
    extended = whoosh.fields.TEXT()
    tags = whoosh.fields.KEYWORD()
    hash = whoosh.fields.ID(unique=True, stored=True)
    href = whoosh.fields.STORED()


def argument_parser():
    parser = argparse.ArgumentParser(
        description="GNOME Search Provider for Pinboard.in")
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help="Enable detailed logging to stderr")
    parser.add_argument('--timeout', metavar='SECONDS', default=10,
                        help="Shut down process after SECONDS inactivity. Default: 10)")
    return parser


class SearchProvider2():
    """<node>
        <interface name="org.gnome.Shell.SearchProvider2">
            <method name="GetInitialResultSet">
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetSubsearchResultSet">
                <arg type="as" name="previous_results" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetResultMetas">
                <arg type="as" name="identifiers" direction="in" />
                <arg type="aa{sv}" name="metas" direction="out" />
            </method>
            <method name="ActivateResult">
                <arg type="s" name="identifier" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
            <method name="LaunchSearch">
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
        </interface>
    </node>"""

    def __init__(self, main_loop, posts, index):
        self.main_loop = main_loop
        self.posts = posts
        self.index = index

        self.pinboard_icon = Gio.ThemedIcon.new('pinboard')

    def GetInitialResultSet(self, terms):
        log.info("Initial search for %s", str(terms))
        self.main_loop.reset_active_timeout()

        hits = self.index.find_search_hits(terms, limit=100)
        results = [hit['hash'] for hit in hits]

        return results

    def GetSubsearchResultSet(self, previous_results, terms):
        log.info("Subsearch for %s", str(terms))
        self.main_loop.reset_active_timeout()
        # We have a backing index, so just search again if we get new terms.
        return self.GetInitialResultSet(terms)

    def GetResultMetas(self, results):
        log.info("Get result metas for %s", results)
        self.main_loop.reset_active_timeout()
        metas = []
        for result_id in results:
            name = self.posts[result_id].get('description', "(no title)")
            description = self.posts[result_id].get('extended', None)
            metas.append({
                'id': GLib.Variant('s', result_id),
                'name': GLib.Variant('s', name),
                'gicon': GLib.Variant('s', self.pinboard_icon.to_string()),
                'description': GLib.Variant('s', description),
            })
        return metas

    def ActivateResult(self, result, terms, timestamp):
        log.info("Activate %s", result)
        self.main_loop.reset_active_timeout()
        href = self.posts[result]['href']
        Gio.AppInfo.launch_default_for_uri(href, None)

    def LaunchSearch(self, terms, timestamp):
        log.info("Launch search %s", terms)
        self.main_loop.reset_active_timeout()
        # This searches all bookmarks, not just the current user, which
        # I think is a lot more fun.
        params = urllib.parse.quote('+'.join(terms))
        href = 'https://pinboard.in/search/?query=' + params
        Gio.AppInfo.launch_default_for_uri(href, None)


class PinboardIndex():
    """Search index managed by Whoosh, created by pinboard_sync.py"""
    def load(self, cachepath):
        log.info("Loading index")

        self.schema = PinboardSchema()
        storage = whoosh.filedb.filestore.FileStorage(cachepath)
        self.index = storage.open_index(schema=self.schema)

        self.parser = self.query_parser()
        # This object opens a number of file handles. Whoosh recommends using
        # it as a context manager to ensure they are closed as soon as
        # possible. We rely on them being released on process exit, instead.
        self.searcher = self.index.searcher()

    def query_parser(self):
        return whoosh.qparser.MultifieldParser(
            ['description', 'extended', 'tags'], schema=self.schema)

    def find_search_hits(self, terms, limit=None):
        """Returns a generator of whoosh.searching.Hit instances."""
        query = self.parser.parse(' '.join(terms))
        yield from self.searcher.search(query, limit=limit)


class MainLoop():
    """Wrapper around GLib main loop which adds an inactivity timeout."""
    def __init__(self):
        self.loop = GLib.MainLoop()

        self.timeout = None
        self.timeout_id = None

    def set_inactive_timeout(self, seconds=None):
        self.timeout = seconds
        self.reset_active_timeout()

    def reset_active_timeout(self):
        if self.timeout_id:
            GLib.source_remove(self.timeout_id)
            self.timeout_id = None
        if self.timeout:
            GLib.timeout_add_seconds(self.timeout, lambda: self._inactive_timeout())

    def _inactive_timeout(self):
        log.info("Exiting due to %i seconds inactivity timer", self.timeout)
        self.loop.quit()
        return GLib.SOURCE_REMOVE

    def run(self):
        self.loop.run()


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    cachepath = pathlib.Path(xdg.BaseDirectory.save_cache_path(CACHE_NAME))

    index = PinboardIndex()
    index.load(cachepath)

    with open(cachepath.joinpath('posts.json')) as f:
        posts = json.load(f)

    loop = MainLoop()

    bus = pydbus.SessionBus()
    dbusname = 'uk.me.afuera.PinboardSearchProvider'
    bus.publish(dbusname, SearchProvider2(loop, posts, index))

    log.info("Waiting for requests on D-Bus name %s", dbusname)
    loop.set_inactive_timeout(int(args.timeout))
    loop.run()


if __name__ == '__main__':
    try:
        main()
    except RuntimeError as e:
        sys.stderr.write("ERROR: {}\n".format(e))
        sys.exit(1)
