# Copyright (C) 2020  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest

import pinboard_sync
import pinboard_search_provider

import json
import pathlib



@pytest.fixture
def post_data():
    test_posts = pathlib.Path(__file__).parent.joinpath('test_posts.json')
    with open(test_posts) as f:
        posts = json.load(f)
    return posts

@pytest.fixture
def index(tmpdir, post_data):
    pinboard_sync.recreate_index(tmpdir, post_data)
    index = pinboard_search_provider.PinboardIndex()
    index.load(tmpdir)
    return index


def test_search(index):
    hits = list(index.find_search_hits(['Twitter']))

    assert len(hits) == 2
    assert hits[0]['hash'] == 'testhash2'
    assert hits[1]['hash'] == 'testhash1'
