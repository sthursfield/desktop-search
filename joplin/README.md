# Joplin search provider for GNOME

This script integrates [Joplin](https://joplinapp.org/) notes with the GNOME
Shell overview, allowing search results from Joplin notes to appear there.

## Installation

To install from source, run these commands.

    meson configure --prefix=/usr
    sudo ninja install

(You can install in a different prefix if you set `$XDG_DATA_DIRS` globally
to include it -- see https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3060).

Next, open Joplin and select the *Tools* -> *Options* menu item. On the
*Web Clipper* tab, make sure the clipper service is enabled, then copy the
API token that is shown under *Advanced Options*.

Create a file named `$HOME/.config/uk.me.afuera.joplin_search_provider.conf`
like this:

    [authentication]
    api_token = 12346789abcdef12346789abcdef  # Replace with your token

## Credits

[Note icon](https://www.flaticon.com/free-icon/note_900082) by
["srip"](https://www.flaticon.com/authors/srip) from
[www.flaticon.com](https://www.flaticon.com/).
