Desktop Search
--------------

This repo contains:

  * A commandline tool for testing GNOME [Search Providers](https://developer.gnome.org/SearchProvider/).
  * A search provider for the Pinboard.in bookmarking site.
