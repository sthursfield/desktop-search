#!/usr/bin/env python3
# Copyright (C) 2020  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import GLib
from gi.repository import Gio

# We use pydbus as GDBus can't publish services from Python,
# according to https://github.com/hcoin/pydbus
import joplin_api
import pydbus
import pydbus.generic
import xdg.BaseDirectory

import argparse
import configparser
import logging
import pathlib
import sys

log = logging.getLogger()

CONFIG_NAME = 'uk.me.afuera.joplin_search_provider.conf'

APP_DESKTOP_NAMES = [
  'appimagekit-joplin.desktop',
  'joplin.desktop',
]


def argument_parser():
    parser = argparse.ArgumentParser(
        description="GNOME Search Provider for Joplin notes")
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help="Enable detailed logging to stderr")
    parser.add_argument('--timeout', metavar='SECONDS', default=10,
                        help="Shut down process after SECONDS inactivity. Default: 10)")
    return parser


def app_info():
    for desktop_name in APP_DESKTOP_NAMES:
        try:
            info = Gio.DesktopAppInfo.new(desktop_name)
            logging.debug("Loaded app info from %s", desktop_name)
            return info
        except TypeError as e:
            # This happens when the constructor returns NULL because the file
            # doesn't exist.
            log.debug("Failed to load app info from %s", desktop_name)


class SearchProvider2():
    """<node>
        <interface name="org.gnome.Shell.SearchProvider2">
            <method name="GetInitialResultSet">
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetSubsearchResultSet">
                <arg type="as" name="previous_results" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="as" name="results" direction="out" />
            </method>
            <method name="GetResultMetas">
                <arg type="as" name="identifiers" direction="in" />
                <arg type="aa{sv}" name="metas" direction="out" />
            </method>
            <method name="ActivateResult">
                <arg type="s" name="identifier" direction="in" />
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
            <method name="LaunchSearch">
                <arg type="as" name="terms" direction="in" />
                <arg type="u" name="timestamp" direction="in" />
            </method>
        </interface>
    </node>"""

    def __init__(self, main_loop, token):
        self.main_loop = main_loop
        self.joplin_api = joplin_api.JoplinApiSync(token)

        self.note_icon = Gio.ThemedIcon.new('uk.me.afuera.JoplinSearchProvider.note')

        self.terms = []

    def _terms_to_query(self, terms):
        query = []
        for term in terms:
            if ':' in term:
                # Pass thru special operators.
                # See https://joplinapp.org/#searching
                query.append(term)
            else:
                # Force prefix matching for each word, so typeahead search
                # works as expected.
                query.append(term + '*')
        return ' '.join(query)

    def GetInitialResultSet(self, terms):
        log.info("Initial search for %s", str(terms))
        self.main_loop.reset_active_timeout()
        self.terms = terms

        query = self._terms_to_query(terms)

        log.info("Running query: %s", query)
        response = self.joplin_api.search(query)
        response.raise_for_status()

        # Return a list of IDs.
        hits = response.json()
        results = [hit['id'] for hit in hits]

        return results

    def GetSubsearchResultSet(self, previous_results, terms):
        log.info("Subsearch for %s", str(terms))
        self.main_loop.reset_active_timeout()
        self.terms = terms

        # Lazy option. Search again on each keypress. Joplin is backed by
        # a local SQLite database so this should be fast enough.
        return self.GetInitialResultSet(terms)

    def _get_snippets(self, terms, body, count=2):
        # Find which line in the note body contains one of the search terms.
        # We show this as the description.
        snippets = []
        terms_lower = [term.lower() for term in terms]
        for line in body.splitlines():
            for term in terms:
                if term in line.lower():
                    snippets.append(line)

                    if len(snippets) >= count:
                        break
        return snippets

    def GetResultMetas(self, results):
        log.info("Get result metas for %s", results)
        self.main_loop.reset_active_timeout()

        metas = []
        for result_id in results:
            response = self.joplin_api.get_note(result_id)
            response.raise_for_status()

            note = response.json()

            name = note['title']
            snippets = self._get_snippets(self.terms, note['body'], count=2)
            description = '\n'.join(snippets)
            metas.append({
                'id': GLib.Variant('s', result_id),
                'name': GLib.Variant('s', name),
                'gicon': GLib.Variant('s', self.note_icon.to_string()),
                'description': GLib.Variant('s', description),
            })
        return metas

    def ActivateResult(self, result, terms, timestamp):
        log.info("Activate %s", result)
        self.main_loop.reset_active_timeout()
        # FIXME: We can only activate the app, not the specific result.
        # See https://discourse.joplinapp.org/t/open-a-note-in-desktop-app-using-commandline/13433/6
        app_info().launch([], None)

    def LaunchSearch(self, terms, timestamp):
        log.info("Launch search %s", terms)
        self.main_loop.reset_active_timeout()
        # FIXME: We can only activate the app, not the specific result.
        # See https://discourse.joplinapp.org/t/open-a-note-in-desktop-app-using-commandline/13433/6
        app_info().launch([], None)


class MainLoop():
    """Wrapper around GLib main loop which adds an inactivity timeout."""
    def __init__(self):
        self.loop = GLib.MainLoop()

        self.timeout = None
        self.timeout_id = None

    def set_inactive_timeout(self, seconds=None):
        self.timeout = seconds
        self.reset_active_timeout()

    def reset_active_timeout(self):
        if self.timeout_id:
            GLib.source_remove(self.timeout_id)
            self.timeout_id = None
        if self.timeout:
            GLib.timeout_add_seconds(self.timeout, lambda: self._inactive_timeout())

    def _inactive_timeout(self):
        log.info("Exiting due to %i seconds inactivity timer", self.timeout)
        self.loop.quit()
        return GLib.SOURCE_REMOVE

    def run(self):
        self.loop.run()


def load_config():
    parser = configparser.ConfigParser()
    for configpath in xdg.BaseDirectory.load_config_paths(CONFIG_NAME):
        log.info("Loading config from %s", configpath)
        parser.read(configpath)
    try:
        api_token = parser['authentication']['api_token']
    except KeyError:
        raise RuntimeError("Did not find 'api_token' in config file. "
                           f"Check ~/.config/{CONFIG_NAME}")
    return api_token


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    api_token = load_config()

    loop = MainLoop()

    bus = pydbus.SessionBus()
    dbusname = 'uk.me.afuera.JoplinSearchProvider'
    bus.publish(dbusname, SearchProvider2(loop, api_token))

    log.info("Waiting for requests on D-Bus name %s", dbusname)
    loop.set_inactive_timeout(int(args.timeout))
    loop.run()


if __name__ == '__main__':
    try:
        main()
    except RuntimeError as e:
        sys.stderr.write("ERROR: {}\n".format(e))
        sys.exit(1)
